# Desafio com lista: Alunos aprovados, recuperação e reprovados.
# Crie um programa que leia o nome e nota de vários alunos. A cada aluno cadastrado o programa deverá perguntar
# se o usuário deseja cadastrar outro aluno ou não.
# Quando o usuário parar de cadastrar alguno o programa deve mostrar.
# a) Quantos alunos cadastrado no total.
# b) Quantos alunos foram aprovados (nota acima de 6) e os nomes.
# c) Quantos alunos estão de recuperação (nota abaixo de 6 e maior que 4) e nomes.
# d) Quantos alunos foram reprovados (nota abaixo de 4) e nomes.

# Inicializa as Listas
alunos = []
aprovados = []
recuperacao = []
reprovados = []

# Cadastra listas de alunos
while True:
    nome = input("Digite o nome do aluno: ")
    nota = float(input("Digite a nota do aluno: ")) 

    alunos.append((nome, nota))

    if nota >= 6:
        aprovados.append((nome, nota))
    elif nota < 6 and nota > 4 :
        recuperacao.append((nome, nota))
    elif nota <= 4:
        reprovados.append((nome, nota))

    print("Digite 's' para dicionar outro aluno ou 'n' para sair. \n")
    opcao = input()
    while opcao != 's' and opcao != 'n':
        print("Digite 's' para dicionar outro aluno ou 'n' para sair. \n")
        opcao = input()

    if opcao == 'n':
        break


# Total de alunos cadastrados
print(f'\n---------------------\nTotal de alunos: {len(alunos)}\n---------------------\n ')

# Lista de alunos aprovados
print('Aprovados:')
for aprov in aprovados:
    print('Aluno:',aprov[0],'-->  Nota:',aprov[1])

# Lista de alunos de recuperação
print('\n') 
print('Recuperação:')
for rec in recuperacao:
    print('Aluno:',rec[0],'-->  Nota:',rec[1])

# Lista de alunos reprovados
print('\n') 
print('Reprovados:')
for repro in reprovados:
    print('Aluno:',repro[0],'-->  Nota:',repro[1])
